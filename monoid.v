From Coq Require Import List.
Import ListNotations.
From Coq Require Import Lia.
Generalizable All Variables.

Section monoid.
  Class Monoid (A : Type) := {
    neutral : A;
    op : A -> A -> A;
    idL : forall a, op neutral a = a;
    idR : forall a, op a neutral = a;
    assoc : forall a b c, op (op a b) c = op a (op b c);
  }.

  Program Instance monoid_nat : Monoid nat :=
    {| neutral := 0;
      op := plus; |}.
  Solve Obligations of monoid_nat with lia.

  Program Instance monoid_pair `{Monoid A, Monoid B} : Monoid (A * B) :=
    {| neutral := (neutral, neutral);
      op := fun '(a, b) '(a', b') => (op a a', op b b'); |}.
  Next Obligation.
    f_equal.
    - apply idL.
    - apply idL.
  Qed.
  Next Obligation.
    f_equal.
    - apply idR.
    - apply idR.
  Qed.
  Next Obligation.
    f_equal.
    - apply assoc.
    - apply assoc.
  Qed.

  Compute (op (1, 2) (2, 3)).

  Fixpoint fold_map `{Monoid B} {A : Type} (f : A -> B) (l : list A) : B :=
    match l with
    | [] => neutral
    | x :: xs => op (f x) (fold_map f xs)
    end.

  About fold_map.

  Definition sum : list nat -> nat :=
    fold_map id.

  Compute (sum [1 ; 2 ; 3]).
End monoid.
