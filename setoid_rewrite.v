From Coq Require Import Lia.
From Coq Require Import List.
Import ListNotations.
From Coq Require Import Relations.
From Coq.Classes Require Import Morphisms RelationClasses.
Generalizable All Variables.

Section map.
  Context {A B : Type}.

  Fixpoint map (f : A -> B) (xs : list A) : list B :=
    match xs with
    | [] => []
    | x :: xs => f x :: map f xs
    end.

  Global Instance map_proper :
    Proper (pointwise_relation A eq ==> eq ==> eq) map.
  Proof.
    intros f g Hfg.
    unfold pointwise_relation in Hfg.
    unfold respectful.
    intros xs ys <-.
    induction xs.
    - simpl. reflexivity.
    - simpl. rewrite IHxs.
      rewrite Hfg. reflexivity.
  Qed.
End map.

Lemma foo xs (P : list nat -> Prop) :
  P (map (fun x => x) xs) -> P (map (fun x => (x + 0)) xs).
Proof.
  intros Hp.
  assert (forall x, x + 0 = x) by lia.
  setoid_rewrite H.
  apply Hp.
Qed.
