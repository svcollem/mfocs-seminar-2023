From Coq Require Import Lia.
From Coq Require Import Relations.
From Coq.Classes Require Import Morphisms RelationClasses.
Require Import Coq.Program.Basics.
Generalizable All Variables.

Section foo.
  Context {A : Type}.
  Context {P : A -> Prop}.
  Context `{Equivalence A eqA}.
  Context `{!Proper (eqA ==> flip impl) P}.

  Lemma foo (x y : A) :
    eqA x y ->
    P y ->
    P x.
  Proof.
    intros rho H'.
    rewrite rho.
    apply H'.
  Qed.
End foo.
