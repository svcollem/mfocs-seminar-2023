\documentclass{beamer}

\usepackage{url}
\usepackage[backend=biber, style=apa]{biblatex}
\addbibresource{references.bib}

\usepackage{ru}

\usepackage{amsmath,amsthm,amssymb,amsfonts}

\usepackage[outputdir=build]{minted}
\setminted{fontsize=\small}

\usepackage[T1]{fontenc}
\usepackage{lmodern}

\usepackage{xpatch,letltxmacro}
\LetLtxMacro{\cminted}{\minted}
\let\endcminted\endminted
\xpretocmd{\cminted}{\RecustomVerbatimEnvironment{Verbatim}{BVerbatim}{}}{}{}

\title{\textbf{Generalized rewriting in Coq}}

\author[Simcha van Collem]{Simcha van Collem \\ Advised by Jules Jacobs}

\institute{Radboud University Nijmegen}
\date{24 January 2023}

\begin{document}

\frame{\titlepage}

\begin{frame}
\frametitle{Outline}

\begin{enumerate}
    \item Type classes in Coq
    \item Generalized rewriting in Coq
\end{enumerate}
\end{frame}

\section{Type classes in Coq}
\begin{frame}
    \begin{alertblock}{First-Class Type Classes}
        \cite{Sozeau_Oury_2008}
    \end{alertblock}
\end{frame}

\begin{frame}
\frametitle{Notation overloading}

\begin{itemize}
    \item $+$ has different meanings in different contexts
    \item Refer to monoid $(M, \varepsilon, \cdot)$ as $M$
    \item Make pen and paper proofs easier
\end{itemize}
    
\end{frame}

\begin{frame}[fragile]
\frametitle{Type classes in Haskell}

\begin{minted}{haskell}
class Monoid a where
    mempty :: a
    (<>) :: a -> a -> a

instance Monoid [a] where
    mempty = []
    (<>) = (++)

foldMap :: Monoid m => (a -> m) -> [a] -> m
foldMap f [] = mempty
foldMap f (x :: xs) = f x <> foldMap f xs
\end{minted}
\end{frame}

\begin{frame}[fragile]
\frametitle{Records}

\begin{columns}
    \begin{column}{0.3\textwidth}
       \begin{minted}{coq}
Record Person := {
  age  : nat;
  name : string;
}
        \end{minted}
    \end{column}

    \begin{column}{0.7\textwidth}
       \begin{minted}{coq}
Inductive Person :=
| Build_Person : nat -> string -> Person


        \end{minted}
    \end{column}
\end{columns}

\begin{center}
\begin{cminted}{coq}

age  : Person -> nat
name : Person -> string

Build_Person 37 "Alice"        : Person
{| age := 42; name := "Bob" |} : Person
\end{cminted}
\end{center}

\end{frame}

\begin{frame}[fragile]
\frametitle{Dependent records}

\begin{minted}{coq}
Record Monoid := {
  carrier : Type;
  neutral : carrier;
  op      : carrier -> carrier -> carrier;
  idL     : forall x, op neutral x = x;
  idR     : forall x, op x neutral = x;
  assoc   : forall x y z, op (op x y) z = op x (op y z);
}
\end{minted}

\end{frame}

\begin{frame}[fragile]
\frametitle{Type classes}

\begin{onlyenv}<1,2>
\begin{minted}{coq}
Class Monoid := {
  carrier : Type;
  neutral : carrier;
  op      : carrier -> carrier -> carrier;
  idL     : forall x, op neutral x = x;
  idR     : forall x, op x neutral = x;
  assoc   : forall x y z, op (op x y) z = op x (op y z);
}
\end{minted}
\end{onlyenv}

\begin{onlyenv}<2>
\begin{minted}{coq}
Definition foo (M : Monoid) : carrier M = nat -> ...
\end{minted}
\end{onlyenv}

\begin{onlyenv}<3>
\begin{minted}{coq}
Class Monoid (carrier : Type) := {
  neutral : carrier;
  op      : carrier -> carrier -> carrier;
  idL     : forall x, op neutral x = x;
  idR     : forall x, op x neutral = x;
  assoc   : forall x y z, op (op x y) z = op x (op y z);
}

Definition foo (M : Monoid nat) : ...
\end{minted}
\end{onlyenv}
\end{frame}

\begin{frame}[fragile]
\frametitle{Type class instance}

\begin{minted}{coq}
Instance monoid_nat : Monoid nat :=
  {| neutral := 0;
     op := plus;
     ... |}.

Instance monoid_pair `{Monoid A, Monoid B} : Monoid (A * B) :=
  {| neutral := (neutral, neutral);
     op := fun '(a, b) '(a', b') => (op a a', op b b');
     ... |}.
\end{minted}
\end{frame}

\begin{frame}[fragile]
\frametitle{Fold map}

\begin{minted}{coq}
Fixpoint fold_map `{Monoid B} {A : Type}
    (f : A -> B) (l : list A) : B :=
  match l with
  | [] => neutral
  | x :: xs => op (f x) (fold_map f xs)
  end.

Definition sum : list (nat * nat) -> nat * nat :=
  fold_map id.
\end{minted}
    
\end{frame}

\begin{frame}[fragile]
\frametitle{Superclasses}

\begin{minted}{coq}
Class Semigroup (A : Type) := {
  op : A -> A -> A;
  assoc : forall a b c, op (op a b) c = op a (op b c);
}.

Class Monoid (A : Type) `{Semigroup A} := {
  neutral : A;
  idL : forall a, op neutral a = a;
  idR : forall a, op a neutral = a;
}.
\end{minted}
\end{frame}

\begin{frame}[fragile]
\frametitle{Substructures}

\begin{minted}{coq}
Class Reflexive {A : Type} (R : relation A) :=
  refl : forall x, R x x.

Class Symmetric {A : Type} (R : relation A) :=
  sym : forall x y, R x y -> R y x.

Class Transitive {A : Type} (R : relation A) :=
  trans : forall x y z, R x y -> R y z -> R x z.

Class Equivalence {A : Type} (R : relation A) :=
  { Equivalence_Reflexive  :> Reflexive A R;
    Equivalence_Symmetric  :> Symmetric A R;
    Equivalence_Transitive :> Transitive A R }.
\end{minted}
\end{frame}

\section{Generalized rewriting in Coq}
\begin{frame}
    \begin{alertblock}{A New Look at Generalized Rewriting in Type Theory}
        \cite{Sozeau_2009}
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]
\frametitle{Leibniz equality is not enough}

\begin{minted}{coq}
Definition set : Type := list nat.
Definition set1 : set := [1 ; 2].
Definition set2 : set := [2 ; 1].

set1 <> set2

Definition eqset : relation set := ...
eqset set1 set2
\end{minted}
\end{frame}

\begin{frame}[fragile]
\frametitle{Proper type class}

\begin{minted}{coq}
Class Proper {A : Type} (R : relation A) (m : A) : Prop :=
  proper : R m m.

Instance reflexive_proper `{Reflexive A R}
  (x : A) : Proper R x.
Proof. reflexivity. Qed.
\end{minted}
\end{frame}

\begin{frame}[fragile]
\frametitle{Respectful}

\begin{minted}{coq}
Definition respectful {A B : Type}
    (R : relation A) (R' : relation B)
    : relation (A -> B) :=
  fun (f g : A -> B) =>
    forall (x y : A),
      R x y -> R' (f x) (g y)

Notation " R ==> R' " := (respectful R R')
  (right associativity, at level 55) : signature_scope.

Definition flip {A : Type} (R : relation A) : relation A :=
  fun (x y : A) => R y x.

Notation " R --> R' " := (respectful (flip R) R')
  (right associativity, at level 55) : signature_scope.
\end{minted}
\end{frame}

\begin{frame}[fragile]
\frametitle{Contrapostitive using Proper}

\begin{minted}{coq}
Definition respectful {A B : Type}
    (R : relation A) (R' : relation B)
    : relation (A -> B) :=
  fun (f g : A -> B) =>
    forall (x y : A),
      R x y -> R' (f x) (g y)

\end{minted}

\begin{onlyenv}<1>
\begin{minted}{coq}
forall (P Q : Prop),
  (Q -> P) -> (~ P -> ~ Q)
\end{minted}
\end{onlyenv}

\begin{onlyenv}<2>
\begin{minted}{coq}
forall (P Q : Prop),
  (impl Q P) -> (impl (~ P) (~ Q))
\end{minted}
\end{onlyenv}

\begin{onlyenv}<3>
\begin{minted}{coq}
forall (P Q : Prop),
  (impl Q P) -> (impl (not P) (not Q))
\end{minted}
\end{onlyenv}

\begin{onlyenv}<4>
\begin{minted}{coq}
Proper (impl --> impl) not
\end{minted}
\end{onlyenv}

\end{frame}

\begin{frame}[fragile]
\frametitle{Proper for union?}

\begin{minted}[fontsize=\tiny]{coq}
Definition respectful {A B : Type}
    (R : relation A) (R' : relation B)
    : relation (A -> B) :=
  fun (f g : A -> B) =>
    forall (x y : A),
      R x y -> R' (f x) (g y)

Instance union_proper : Proper (eqset ==> eqset ==> eqset) union.
\end{minted}

\begin{onlyenv}<2->
\begin{minted}[fontsize=\footnotesize]{coq}
eqset ==> eqset
  =
fun (f g : set -> set) => forall (s s' : set),
  eqset s s' -> eqset (f s) (g s')
\end{minted}
\end{onlyenv}

\begin{onlyenv}<3>
\begin{minted}[fontsize=\footnotesize]{coq}

eqset ==> (eqset ==> eqset)
  =
fun (f g : set -> (set -> set)) => forall (t t' : set),
  eqset t t' -> (eqset ==> eqset) (f t) (g t')
\end{minted}
\end{onlyenv}

\begin{onlyenv}<4>
\begin{minted}[fontsize=\footnotesize]{coq}

eqset ==> (eqset ==> eqset)
  =
fun (f g : set -> (set -> set)) => forall (t t' : set),
  eqset t t' -> forall (s s' : set),
    eqset s s' -> eqset ((f t) s) ((g t') s')
\end{minted}
\end{onlyenv}

\begin{onlyenv}<5>
\begin{minted}[fontsize=\footnotesize]{coq}

(eqset ==> (eqset ==> eqset)) union union
  =
forall (t t' : set),
  eqset t t' -> forall (s s' : set),
    eqset s s' -> eqset (union t s) (union t' s')
\end{minted}
\end{onlyenv}
\end{frame}

\begin{frame}
\frametitle{How to read Propers}

Given
\begin{itemize}
  \item \mintinline{coq}{f : A1 -> A2 -> ... -> An -> B}
  \item \mintinline{coq}{Ri : relation Ai} for each \mintinline{coq}{i}
  \item \mintinline{coq}{R : relation B}
  \item \mintinline{coq}{xi : Ai} and \mintinline{coq}{yi : Ai} for each \mintinline{coq}{i}
\end{itemize}
Then \mintinline{coq}{Proper (R1 ==> R2 ==> ... ==> Rn ==> R) f} represents the fact that
\begin{itemize}
  \item If \mintinline{coq}{Ri xi yi} for each \mintinline{coq}{i},
  \item then \mintinline{coq}{R (f x1 ... xn) (f y1 ... yn)}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Rewrite judgement}
Constraints of the form $?_x \, : \, \Gamma \vdash A$

\begin{onlyenv}<2->
Given a rewriting lemma $\rho$, the judgement
\begin{gather*}
  \Gamma \mid \psi \vdash t \rightsquigarrow_p^R t' \dashv \psi'
\end{gather*}
states that in environment $\Gamma$ with constraints $\psi$, $t$ is rewritten to $t'$
with respect to relation $R$ and $p$ a proof of $R\ t\ t'$,
generating new constraints $\psi'$.
\end{onlyenv}

\begin{onlyenv}<3->
Given a goal $\Gamma \vdash t$ and a rewrite lemma $\rho$,
we want to find the following judgement
\begin{gather*}
  \Gamma \mid \emptyset \vdash t \rightsquigarrow_p^{\textsf{flip impl}} t' \dashv \psi'
\end{gather*}
which reduces the goal to $\Gamma \vdash t'$.
\end{onlyenv}
\end{frame}

\begin{frame}
\frametitle{Rewrite example}

\begin{itemize}
  \item Rewrite goal \mintinline{coq}|P x| using rewrite lemma \mintinline[escapeinside=||,mathescape=true]{coq}{|$\rho$| : eqA x y}
  \item Goal: find judgement of the form $\Gamma \mid \emptyset \vdash P\ x \rightsquigarrow_{\dots}^{\textsf{flip impl}} \dots \dashv \psi'$
  \item<2-> 
  $\Gamma \mid \emptyset \vdash P \rightsquigarrow_{?_m}^{?_S} P \dashv$ \\
  $\phantom{aa}\underbrace{\{?_S : \Gamma \vdash \textsf{relation (A -> Prop)}, ?_m : \Gamma \vdash \textsf{Proper}\,?_S\,P\}}_{\psi}$
  \item<3-> $\Gamma \mid \psi \vdash x \rightsquigarrow_\rho^{\textsf{eqA}} y \dashv \psi$
  \item<4->
   $\Gamma \mid \emptyset \vdash P\,x \rightsquigarrow_{?m\,x\,y\,\rho}^{?_T} P\,y \dashv$ \\
   $\phantom{aa}\{?_T : \Gamma \vdash \textsf{relation Prop}, ?_m : \Gamma \vdash \textsf{Proper}\,(\textsf{eqA ==> }?_T)\,P\}$
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{References}
    \printbibliography
\end{frame}

\end{document}
