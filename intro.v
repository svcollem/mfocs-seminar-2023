From Coq Require Import Lia.
From Coq Require Import Relations.
From Coq.Classes Require Import Morphisms RelationClasses.
Generalizable All Variables.

Parameter set : Type.
Parameter eqset : relation set.

Axiom eqsetequiv : Equivalence eqset.
Global Existing Instance eqsetequiv.

Parameter empty : set.
Parameter union : set -> set -> set.

Axiom union_empty : forall s, eqset (union s empty) s.
Axiom union_idem  : forall s, eqset (union s s) s.

Axiom union_compat : forall (s s' : set),
  eqset s s' ->
  forall (t t' : set),
    eqset t t' ->
    eqset (union s t) (union s' t').

Goal forall s, eqset (union (union s empty) s) s.
Proof.
  intros s. transitivity (union s s).
  - apply union_compat.
    + apply union_empty.
    + reflexivity.
  - apply union_idem.
Qed.

Global Instance union_proper : Proper (eqset ==> eqset ==> eqset) union.
Proof. Admitted.

Goal forall s, eqset (union (union s empty) s) s.
Proof.
  intros s.
  rewrite union_empty.
  rewrite union_idem.
  reflexivity.
Qed.
