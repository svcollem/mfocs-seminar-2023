From Coq Require Import Lia.
From Coq Require Import Relations.
From Coq.Classes Require Import Morphisms RelationClasses.

Global Instance plus_monotone :
  Proper (le ==> le ==> le) Nat.add.
Proof.
  intros m n Hmn.
  intros k l Hkl.
  apply Plus.plus_le_compat.
  - apply Hmn.
  - apply Hkl.
Qed.

Lemma foo (x y : nat) :
  x <= y ->
  x + 2 <= y + 5.
Proof.
  intros H.
  rewrite H.
  Admitted.
