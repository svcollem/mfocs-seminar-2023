From Coq Require Import Lia.
From Coq Require Import Relations.
From Coq.Program Require Import Basics.
From Coq.Classes Require Import Morphisms RelationClasses.
Generalizable All Variables.

Class Equivalence {A : Type} (R : relation A) := {
  Equivalence_Reflexive  :> Reflexive R;
  Equivalence_Symmetric  :> Symmetric R;
  Equivalence_Transitive :> Transitive R }.

Global Instance reflexive_proper `{Reflexive A R} (x : A) : Proper R x.
Proof. unfold Proper. reflexivity. Qed.

Global Instance not_proper : Proper (impl --> impl) not.
Proof.
  intros P Q HQP HNP HQ.
  apply HNP. apply HQP. apply HQ.
Qed.
