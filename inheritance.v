From Coq Require Import Lia.
From Coq Require Import Relations.
Generalizable All Variables.

Section superclasses.
  Class Semigroup (A : Type) := {
    op : A -> A -> A;
    assoc : forall a b c, op (op a b) c = op a (op b c);
  }.

  Class Monoid (A : Type) `{Semigroup A} := {
    neutral : A;
    idL : forall a, op neutral a = a;
    idR : forall a, op a neutral = a;
  }.

  Program Instance semigroup_nat : Semigroup nat :=
    {| op := plus |}.
  Solve Obligations of semigroup_nat with lia.

  Program Instance monoid_nat : Monoid nat :=
    {| neutral := 0 |}.

End superclasses.

Section substructures.

  Class Reflexive {A : Type} (R : relation A) :=
    refl : forall x, R x x.

  Class Symmetric {A : Type} (R : relation A) :=
    sym : forall x y, R x y -> R y x.

  Class Transitive {A : Type} (R : relation A) :=
    trans : forall x y z, R x y -> R y z -> R x z.

  Class Equivalence {A : Type} (R : relation A) :=
    { Equivalence_Reflexive :> Reflexive R;
      Equivalence_Symmetric :> Symmetric R;
      Equivalence_Transitive :> Transitive R }.

  Instance eq_equivalence {A : Type} : Equivalence (@eq A).
  Proof.
    split.
    - intros x. reflexivity.
    - intros x y H. rewrite H. reflexivity.
    - intros x y z H H'.
      rewrite H. rewrite H'.
      reflexivity.
  Qed.

  Goal Reflexive (@eq nat).
  Proof. apply _. Qed.

End substructures.
