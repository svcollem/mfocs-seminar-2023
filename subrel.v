From Coq Require Import Lia.
From Coq Require Import Relations.
From Coq.Classes Require Import Morphisms RelationClasses.
Generalizable All Variables.

Parameter set : Type.
Parameter eqset  : relation set.

Axiom eqsetequiv : Equivalence eqset.
Global Existing Instance eqsetequiv.

Parameter empty : set.
Parameter union : set -> set -> set.

Axiom union_empty : forall s, eqset (union s empty) s.
Axiom union_idem  : forall s, eqset (union s s) s.

Axiom union_compat : forall (s s' : set),
  eqset s s' ->
  forall (t t' : set),
    eqset t t' ->
    eqset (union s t) (union s' t').

Global Instance union_proper :
  Proper (eqset ==> eqset ==> eqset) union.
Proof.
  unfold Proper, respectful.
  exact union_compat.
Qed.



Parameter subset : relation set.

Parameter subset_po : PreOrder subset.
Global Existing Instance subset_po.
Parameter subset_partial : PartialOrder eqset subset.
Global Existing Instance subset_partial.

Parameter diff  : set -> set -> set.

Axiom diff_compat : forall (s s' : set),
  subset s s' ->
  forall (t t' : set),
    subset t' t ->
    subset (diff s t) (diff s' t').

Global Instance diff_proper :
  Proper (subset ==> subset --> subset) diff.
Proof. exact diff_compat. Qed.

Goal forall s t u, eqset s t -> subset (diff s u) (diff t u).
Proof.
  intros s t u H.
  Fail rewrite H.
  Admitted.

Parameter eqset_subset : subrelation eqset subset.
Global Existing Instance eqset_subset.

Goal forall s t u, eqset s t -> subset (diff s u) (diff t u).
Proof.
  intros s t u H.
  rewrite H.
  reflexivity.
Qed.
